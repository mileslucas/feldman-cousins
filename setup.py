from setuptools import setup, find_packages

setup(name='fc',
      version='0.2',
      description='Implementation of the Feldmen-Cousins method for parameter fitting intervals',
      url='http://github.com/mileslucas/feldman-cousins',
      author='Miles Lucas',
      author_email='mdlucas@iastate.edu',
      license='MIT',
      packages=find_packages(),
      install_requires=[
        'numpy',
          'scipy',
          'progressbar2'
      ],
      zip_safe=False)

# Feldman-Cousins
[![master status](https://gitlab.com/mileslucas/feldman-cousins/badges/master/pipeline.svg)](https://gitlab.com/mileslucas/feldman-cousins/commits/master) 
[![coverage report](https://gitlab.com/mileslucas/feldman-cousins/badges/master/coverage.svg)](https://gitlab.com/mileslucas/feldman-cousins/commits/master)
[![develop status](https://gitlab.com/mileslucas/feldman-cousins/badges/develop/pipeline.svg)](https://gitlab.com/mileslucas/feldman-cousins/commits/develop) 
[![coverage report](https://gitlab.com/mileslucas/feldman-cousins/badges/develop/coverage.svg)](https://gitlab.com/mileslucas/feldman-cousins/commits/develop)

This module will seek to provide functionality for the Feldman-Cousins statistical modeling framework.

## Getting Started

To get set up using this package, first download the source or use
```
$ git clone https://github.com/mileslucas/feldman-cousins
```
Then, run setup by using
```
$ pip install .
```
or
```
$ python setup.py install
```
This will install the package and can then be imported using
```
In[1]: import fc
```
## Authors

* **Miles Lucas** - *Iowa State University*

See also the list of [contributors](https://github.com/mileslucas/feldman-cousins/contributors) who participated in this project.

## Acknowledgments
Special thanks to Dr. Massimo Marengo for his guidance in this project, as well as the Iowa Space Grant Association for their sponsorship of my work with Massimo.

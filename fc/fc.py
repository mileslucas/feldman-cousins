import logging
import time
from collections import Iterable
from inspect import signature

import numpy as np
from progressbar import ProgressBar
from scipy.optimize import minimize
from scipy.stats import percentileofscore

logging.basicConfig(level=logging.INFO)

def _get_parameter_space(p1_range, p2_range, grid_size):
    '''
    Returns line spaces for each parameter
    '''
    if isinstance(grid_size, Iterable):
        p1_space = np.linspace(*p1_range, grid_size[0])
        p2_space = np.linspace(*p2_range, grid_size[1])

    else:
        p1_space = np.linspace(*p1_range, grid_size)
        p2_space = np.linspace(*p2_range, grid_size)

    return p1_space, p2_space


def _setup_initial_guess(n, lims):
    '''
    Sets up the bounds and guesses of parameters for use in minimization function
    '''
    if lims is None or len(lims) == 0:
        return np.ones(n)
    elif n != len(lims):
        raise ValueError(
            'Must match a limit for each parameter. If only one parameter, make sure limits are a list with one tuple')

    p0 = []
    for pair in lims:
        if pair[0] is None and pair[1] is None:
            p0.append(1)
        elif pair[0] is None:
            p0.append(pair[1])
        elif pair[1] is None:
            p0.append(pair[0])
        else:
            p0.append(np.average(pair))

    return p0

def fit(fun, x, data, p1_range, p2_range, sigma=1, grid_size=20, nuis_lims=None, **min_kwargs):
    '''
    Fits a model with only two parameters, p1 and p2 using the feldman-cousins method.
    This creates a parameter space and evaluates a classic chi-square value for each
    point in the parameter space. Then, using the chi2 values and the minimum chi2 value,
    creates the delta chi-square space and masks it to only include values within a critical
    value of chi2, usually 2 sigma or 6.18.

    Parameters
    ----------
    fun : callable
        The callable that is used as the model for this fit, must have the form
        fun(x, p1, p2, *nuis)
    x : array_like
        The independent variable corresponding with the data
    data : array_like
        The data that is being fitted by the model
    p1_range : array_like
        The range [a,b) to fit parameter 1
    p2_range : array_like
        The range [a,b) to fit parameter 2
    sigma : int or array_like, optional
        The error as a constant (homoscedastic) or the error for every data point
        (heteroscedastic) (default = 1)
    grid_size : int or array_like, optional
        the number of grid points per parameter. IE, if parameter p1 has range -10
        to 10, and grid_size is 100, the step size for p1 will be 0.2. If an array
        is passed, the first value will correspond to p1 size, the second value
        will correspond to p2 size (default = 20)
    nuis_lims : (N, 2) array_like, optional`
        For each nuisance parameter, a limit can be set for the fitting of the nuisance parameters.  If set for one,
        there must be an entry for each nuisance parameter. For unbounded, use None. They will
        default to (None, None). The format is [ (low, upp), (low, upp), ... ]

    Returns
    -------
    result : fit_result
        A special class that holds all the relevant data for a fit using this method
        See fit_result's documentation for more information

    Examples
    --------
    >>> def model(x, a, b):
    ...     return a * x + b
    >>> x = np.linspace(0, 10)
    >>> params = (4, -2)
    >>> data = model(x, *params) + np.random.normal(0, 1, len(x))
    >>> ranges = [(2, 5), (-3, 0)]
    >>> res = fc.fit(model, x, data, *ranges)
    '''

    if not callable(fun):
        raise ValueError('fun must be a callable with form fun(x, p1, p2, *nuis)')

    if (len(x) != len(data)):
        raise ValueError('x and data must have same length')

    if (len(p1_range) != 2):
        raise ValueError(
            'p1_range must be two values corresponding to the minimum and maximum of the parameter range')

    if (len(p2_range) != 2):
        raise ValueError(
            'p2_range must be two values corresponding to the minimum and maximum of the parameter range')

    # Get number of nuisance parameters
    n_nuis = len(signature(fun).parameters) - 3

    p1_space, p2_space = _get_parameter_space(p1_range, p2_range, grid_size)
    chi2 = np.empty((p1_space.size, p2_space.size))
    p0 = _setup_initial_guess(n_nuis, nuis_lims)
    nuis_params = None
    if n_nuis > 0:
        nuis_params = np.empty((*chi2.shape, n_nuis))

    for p1_idx, p1 in enumerate(p1_space):
        for p2_idx, p2 in enumerate(p2_space):

            if n_nuis > 0:
                def chi2_func(nuis):
                    r = fun(x, p1, p2, *nuis) - data
                    return np.sum((r / sigma) ** 2)
                    
                opt = minimize(chi2_func, p0, bounds=nuis_lims, **min_kwargs)
                nuis_params[p1_idx, p2_idx] = opt.x
                chi2[p1_idx, p2_idx] = opt.fun
            else:
                r = fun(x, p1, p2) - data
                chi2[p1_idx, p2_idx] = np.sum((r / sigma) ** 2)

    delta_chi2 = chi2 - chi2.min()
    return fit_result(fun, x, data, sigma, p1_range, p2_range, p1_space, p2_space, delta_chi2, nuis_params)

class fit_result():

    def __init__(self, fun, x, data, sigma, p1_range, p2_range, p1_space, p2_space, delta_chi2, nuis_params):
        '''

        Parameters
        ----------
        fun : callable
            The callable that is used as the model for this fit, must have the form
            fun(x, p1, p2, *nuis)
        x : array_like
            The independent variable corresponding with the data
        data : array_like
            The data that is being fitted by the model
        p1_range : array_like
            The range [a,b) to fit parameter 1
        p2_range : array_like
            The range [a,b) to fit parameter 2
        p1_space : array_like
            The linespace for parameter 1
        p2_space : array_like
            The linespace for parameter 2
        sigma : int or array_like, optional
            The error as a constant (homoscedastic) or the error for every data point
            (heteroscedastic) (default = 1)
        delta_chi2 : (N, N) ndarray
            The delta chi2 surface from .. fit
        nuis_params : (N, N, M) ndarray
            The nuisance paramters fit by .. fit for each point in parameter space

        Attributes
        ----------
        fc_surfaces : (N, M, M) ndarray
            The result from each experiment in the fc fit
        '''
        self.x = x
        self.data = data
        self.sigma = sigma
        self.fun = fun
        self.p1_range = p1_range
        self.p2_range = p2_range
        self.p1_space = p1_space
        self.p2_space = p2_space
        self._p1_mindx, self._p2_mindx = np.unravel_index(
            delta_chi2.argmin(), delta_chi2.shape)
        if nuis_params is None:
            self.params = [p1_space[self._p1_mindx], p2_space[self._p2_mindx]]
            self.best_fit = fun(x, *self.params)
        else:
            self.params = [p1_space[self._p1_mindx], p2_space[self._p2_mindx],
                           *nuis_params[self._p1_mindx, self._p2_mindx]]
            self.best_fit = fun(x, *self.params)
        self.residuals = self.best_fit - data
        self.dchi2_space = delta_chi2
        self.nuis_params = nuis_params
        self.fc_surfaces = None


    def fc(self, n_exp=1000, err_model=None):
        '''
        Perform the Feldman-Cousin resampling experiments

        Parameters
        ----------
        n_exp : int
            The number of monte-carlo samples to generate (default=1000)
        err_model : callable, optional
            The error model to be used for monte carlo synthetic data creation. This should have the same signature as
            the normal function err_model(x, p1, p2, *nuis). If this is not set, the default will be an additive,
            normal, stochastic error y = f(x) + e (default=None)

        Returns
        -------
        dchi2_surfaces : (N, G, G) ndarray
            an array with delta chi2 surfaces for each experiment
        '''

        if err_model is None:
            def err_model(x, *params):
                noise = np.random.normal(0, self.sigma, x.size)
                return self.fun(x, *params) + noise

        dchi2_surfaces = np.empty((n_exp, *self.dchi2_space.shape))

        bounds = [self.p1_range, self.p2_range]
        p0 = _setup_initial_guess(2, bounds)

        begin = time.clock()
        bar = ProgressBar()
        for exper in bar(range(n_exp)):
            # For each point in the parameter space
            for p1_idx, p1 in enumerate(self.p1_space):
                for p2_idx, p2 in enumerate(self.p2_space):
                    nuis = [] if self.nuis_params is None else self.nuis_params[p1_idx, p2_idx]
                    model = self.fun(self.x, p1, p2, *nuis)
                    synth = err_model(self.x, p1, p2, *nuis)

                    # # Fit the synthetic data to the model function with bounds
                    def chi2_func(p):
                        y = self.fun(self.x, p[0], p[1], *nuis)
                        r = y - synth
                        return np.sum((r / self.sigma) ** 2)

                    opt = minimize(chi2_func, p0, bounds=bounds)
                    min_chi2 = opt.fun
                    # Calculate the chi2 of the noise vs. the pure model for this dataset
                    r_other = synth - model
                    other_chi2 = np.sum((r_other / self.sigma) ** 2)
                    # Calculate the delta chi2
                    dchi2_surfaces[exper, p1_idx, p2_idx] = other_chi2 - min_chi2
        end = time.clock()
        logging.info('Time elapsed for {} experiments: {:.4f}s'.format(n_exp, end-begin))
        self.fc_surfaces = dchi2_surfaces
        return self.fc_surfaces

    def get_surface(self, percentile):
        '''
        Get the final masked surface that represents the percentile-th confidence surface for the parameters p1 and p2

        Parameters
        ----------
        percentile : int or float
             The percentile for use int the FC method. This corresponds to the confidence level of the parameter space.

        Returns
        -------
        crit_surf : (G, G) ndarray
            A 2-d array of the masked parameter space.
        '''

        if self.fc_surfaces is None:
            raise ValueError('Must run fc.fc() before getting surfaces')

        surf = np.percentile(self.fc_surfaces, percentile, axis=0)
        return np.ma.masked_greater(self.dchi2_space, surf)

    def get_confidence_surface(self):
        '''
        Gets the confidence for all dchi2 values. The way this surface works is it allows us to see how much confidence
        we need to include a certain point in parameter space. So, we could see that the edges have close to 0 confidence
        and the peak/s will be close to 1.00 confidence. To the the confidence surface, use get_surface which does the
        equivalent of doing a slice on the surface in the parameter plane at confidence 1-alpha.

        Returns
        -------
        conf_surf : (G, G) ndarray
            A 2-d array of the confidence values
        '''
        if self.fc_surfaces is None:
            raise ValueError('Must run fc.fc() before getting surfaces')

        conf = np.empty(self.dchi2_space.shape)
        for p1_idx, p1 in enumerate(self.p1_space):
            for p2_idx, p2 in enumerate(self.p2_space):
                score = percentileofscore(self.fc_surfaces[:, p1_idx, p2_idx], self.dchi2_space[p1_idx, p2_idx])
                conf[p1_idx, p2_idx] = 1 - score / 100

        return conf

if __name__ == '__main__':
    pass

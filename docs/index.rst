.. fc documentation master file, created by
    sphinx-quickstart on Thu Dec 28 02:21:42 2017.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

***
fc
***

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    chi2fit
    api


Introduction
============

Prerequisites
-------------
The prerequisites are

* numpy
* scipy
* progressbar2


Installation
-------------

To install, first clone the repository from Git::

    git clone https://github.com/mileslucas/feldman-cousins

then to install::

    cd feldman-cousins
    pip install .


Usage
-----
Chi2 fitting
++++++++++++
To run the fitting there needs to be a model function, x data, y data, and parameter ranges. The signature of
the model function `has` to be ``func(x, p1, p2, *nuis)``. For our simple example, lets assume we are trying to fit a
line with only two parameters. The chi2 fitting routine can be run like this::

    import fc
    import numpy as np

    def model(x, m, b):
        return m * x + b

    x = np.linspace(0, 10)
    data = model(x, -2, 4) + np.random.normal(0, 1, len(x))
    ranges = [(-10, 0), (0, 20)]
    res = fc.fit(model, x, data, *ranges)

This will return a :class:`fc.fc.fit_result`.

FC fitting
++++++++++
Once we have our fit result, we can run the Feldman-Cousin fitting routine. This will do a monte-carlo resampling process
that creates a distribution of chi2 values for each parameter in parameter space. To run this, just take the fit result
from the chi2 fitting and use::

    res.fc()

To get the fc surfaces for the final confidence interval use :func:`fc.fc.fit_result.get_surface` or
:func:`fc.fc.fit_result.get_confidence_surface`. To see what the 95% confidence interval in parameter space is::

    crit_surf = res.get_surface(95)
    import matplotlib as plt
    plt.imshow(crit_surf, origin='lower', extent=(ranges[1], ranges[0]))
    plt.ylabel('m')
    plt.xlabel('b')
    plt.colorbar()
    plt.show()






Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

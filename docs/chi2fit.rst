************
Chi2 fitting
************

The basis of the Feldman-Cousin method is the delta-chi2 surface. This surface is simply a way of giving a statistic
of fit to every parameter in a grid I will call parameter space.

Parameter Space
---------------
This whole module was made with the intention of curve fitting two variables of a function with astrostatisitcs in mind.
These two parameters will be analyzed over a grid which consists of every permutation of the two parameters. Because this
is a grid, there has to be ranges on each side and the parameters cannot be unbounded.

This parameter space will be used frequently in the code, especially during the fc method. This adds a runtime of `O(n^2)`
so the larger the grid the slower the code is, quadratically.
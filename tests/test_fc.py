import numpy as np
import pytest

from fc import fc


@pytest.fixture(scope='module')
def res_nuis():
    x = np.linspace(-5, 5, 100)

    def model(x, a, b, c):
        return a * x ** 2 + b * x + c

    data = model(x, 3, 4, 1) + np.random.normal(0, 1, x.size)
    range_ = [[2.93, 3.075], [3.8, 4.2]]
    ext = (*range_[1], *range_[0])
    return fc.fit(model, x, data, *range_, grid_size=[10, 11], nuis_lims=[(0, 2)])


@pytest.fixture(scope='module')
def res():
    x = np.linspace(-5, 5, 100)

    def model(x, a, b):
        return a * x ** 2 + b * x

    data = model(x, 3, 4) + np.random.normal(0, 1, x.size)
    range_ = [[2.93, 3.075], [3.8, 4.2]]
    ext = (*range_[1], *range_[0])
    return fc.fit(model, x, data, *range_, grid_size=[10, 11])

def test_range(res):
    expected = [3.075 - 2.93, 4.2 - 3.8]
    actual = [res.p1_space.max() - res.p1_space.min(),
              res.p2_space.max() - res.p2_space.min()]
    assert actual == expected


def test_min(res):
    assert res.dchi2_space.min() == 0


def test_shape(res):
    expected = (10, 11)
    assert res.dchi2_space.shape == expected


def test_accuracy(res):
    expected = [3, 4]
    assert expected == pytest.approx(res.params, .5)


def test_accuracy_nuis(res_nuis):
    expected = [3, 4, 1]
    assert expected == pytest.approx(res_nuis.params, .5)

@pytest.mark.skip('To test add _setup_initial_guess to the fc/__init__.py file')
def test_setup_bounds():
    lims = [(0, 4), (-np.inf, np.inf), (-1, np.inf)]
    p0 = [2, 1, -1]
    p0_out, lims_out = fc._setup_bounds(len(lims), lims)
    assert p0 == pytest.approx(p0_out)


@pytest.mark.skip('To test add _setup_initial_guess to the fc/__init__.py file')
def test_setup_bounds_2():
    lims = None
    p0 = [1, 1, 1]
    p0_out, lims_out = fc._setup_bounds(3, lims)
    assert p0 == pytest.approx(p0_out)

@pytest.mark.skip('No good way to test this')
def test_autorange(res):
    p, e = res.autoParams()

def test_fc_runs(res):
    res.fc(n_exp=1)


def test_fc_nuis_runs(res_nuis):
    res_nuis.fc(n_exp=1)
